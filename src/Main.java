import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.Cursor;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.*;

public class Main extends Application {

    @Override
    public void init() throws Exception{
        System.out.println("This Happens Before start method");// you can use it for loading resources , img etc
    }
    //template
    @Override
    public void start(Stage stage) throws Exception {
        //Stage is the window our app is going to use
        stage.setTitle("Ultimate Chess - ANDI && WIK && KUBA");
        stage.setWidth(1000);
        stage.setHeight(800);

        Square[][] matrix = new Square [8][8];
        for (int i =0; i<8;i++){
            for (int j= 0; j<8; j++){

            }
        }

        VBox parent = new VBox();//root node
        Scene scene1 = new Scene(parent);

        Label label1 = new Label("Ultimate Chess"); //child node
        Label label2 = new Label("Version 0.0.1"); //child node
        parent.getChildren().addAll(label1, label2);

        scene1.setCursor(Cursor.CLOSED_HAND);
        stage.setScene(scene1);
        stage.show();//you gotta show your stage

//        Stage stage2 = new Stage();
//        stage2.setTitle("Second Stage");
//
//        //setting the size of the window
//        stage2.setWidth(700);
//        stage2.setHeight(400);
//
//        //setting the location of the window
//        stage2.setX(400);
//        stage2.setY(300);
//
//        stage2.show();
//
//        //modality
//        Stage newWindow = new Stage();
////        newWindow.initModality(Modality.APPLICATION_MODAL);//other windows cannot be open until this one is closed
////        newWindow.initModality(Modality.NONE); //default value
//        newWindow.initModality(Modality.WINDOW_MODAL); //it can own other window
//        newWindow.initOwner(stage2);
//
//        //Style - invisible or not or connected with bar X <>_ bar
//        newWindow.initStyle(StageStyle.UNIFIED);
//        newWindow.show();
//
//        stage.toFront();//send window to the front
//        stage.setFullScreen(true);// set th full screen of an app
    }

    @Override
    public void stop() throws Exception {
        System.out.println("This Happens when the app is closed/stopped ");//use it to save work data etc
    }

    public static void main(String[] args){

        System.out.println("Hello World !!!");
        Application.launch(args);

    }
}
